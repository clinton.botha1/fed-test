import ProductCarousel from "../../components/product-carousel/product-carousel";

import styles from "./product-detail.module.scss";

export async function getStaticPaths() {
  const response = await fetch(
    "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
  );
  const data = await response.json();

  const paths = data.products.map((item) => {
    return {
      params: { id: item.productId.toString() },
    };
  });

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps(context) {
  const id = context.params.id;
  const response = await fetch(
    "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
  );
  const data = await response.json();

  return {
    props: { data },
  };
}

const ProductDetail = ({ data }) => {
  return (
    <div>
      <h1 className={styles.h1}>{data.title}</h1>
      <div className={styles.content}>
        <div className={styles.productCarousel}>
          <ProductCarousel image={data.media.images.urls[0]} />
        </div>
        <div className={styles.priceInformation}>
          <div className={styles.priceInformation}>
            <div className={styles.price}>&pound;{data.price.now}</div>
            <div className={styles.offers}>{data.displaySpecialOffer}</div>
            <div className={styles.guarantee}>{data.additionalServices.includedServices}</div>
          </div>
        </div>
        <div className={styles.productInformation}>
          <h3>Product information</h3>
        </div>
        <div>
          <h3>Product specification</h3>
          <ul>
            {data.details.features[0].attributes.map((item) => (
              <li>
                <div>{item.name}</div>
                <div>{item.value}</div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
