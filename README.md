# UI Dev Technical Test - Dishwasher App

## Introduction

In this pack you will find 3 images that show the desired screen layout for the app

- Product Page Portrait.png
- Product Page Landscape.png
- Product Grid.png
- ProductGridAPI
- ProductPageAPI

The aim of this test is for the applicant to create a website that sells Dishwashers. We would like to see the following:

- The website should work on a tablet such as an iPad (768 x 1024 px), in landscape and
  portrait mode.
- The use of third party Code/SDKs is allowed, but you should be able to explain why
  you have chosen the third party.
- We’d like to see a TDD approach to writing the app, using a popular unit test framework of
  your choice.
- Committing your code constantly to a GitHub account.
- Put all your assumptions, notes and instructions into your GitHub README.md.
- We’re not looking for a ‘pixel perfect’ app, we are looking at your style of coding, and how
  you’ve approached this.
- Return your code to us by sending a link to the public Gitub repository that contains the code. We will not review code that is not in a Github repository.

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John
Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that
are currently available for customers to buy. For this exercise we’d be looking at only displaying
the first 20 results returned by the API.

The layout of the screen should look like the attached image - Product Grid.png Details of the
API Can be found in the document ProductGridAPI

### Product Page - (Optional)

When a dishwasher is tapped, a new screen is displayed showing the dishwasher’s details. The
screen layout is defined in the following two files

- Portrait Mode - Product Page Portrait.png
- Landscape Mode - Product Page Landscape.png

Details of the API Can be found in the attached document ProductPageAPI, We’d like to see the Product page display the details for the product page, we don’t expect you
to provide a further solution to drill down to the reviews, or add to basket functionality.

---

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
